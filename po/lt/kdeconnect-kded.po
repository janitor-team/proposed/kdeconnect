# Lithuanian translations for l package.
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the l package.
# Automatically generated, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-05-24 00:16+0000\n"
"PO-Revision-Date: 2021-08-21 13:17+0300\n"
"Last-Translator: Moo\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Moo"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "<>"

#: kdeconnectd.cpp:51
#, kde-format
msgid ""
"Pairing request from %1\n"
"Key: %2..."
msgstr ""
"Suporavimo užklausa iš %1\n"
"Raktas: %2..."

#: kdeconnectd.cpp:52
#, kde-format
msgid "Open"
msgstr "Atverti"

#: kdeconnectd.cpp:53
#, kde-format
msgid "Accept"
msgstr "Priimti"

#: kdeconnectd.cpp:53
#, kde-format
msgid "Reject"
msgstr "Atmesti"

#: kdeconnectd.cpp:53
#, kde-format
msgid "View key"
msgstr "Rodyti raktą"

#: kdeconnectd.cpp:135 kdeconnectd.cpp:137
#, kde-format
msgid "KDE Connect Daemon"
msgstr "KDE Connect tarnyba"

#: kdeconnectd.cpp:148
#, kde-format
msgid "Replace an existing instance"
msgstr "Pakeisti esamą egzempliorių"
