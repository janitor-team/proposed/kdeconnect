# Lithuanian translations for l package.
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the l package.
#
# Automatically generated, 2014.
# Liudas Ališauskas <liudas@aksioma.lt>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: l 10n\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-10-13 00:17+0000\n"
"PO-Revision-Date: 2021-01-31 21:48+0200\n"
"Last-Translator: Moo\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 2.4.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Moo"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "<>"

#: kcm.cpp:34
#, kde-format
msgid "KDE Connect Settings"
msgstr "KDE Connect nuostatos"

#: kcm.cpp:36
#, kde-format
msgid "KDE Connect Settings module"
msgstr "KDE Connect nuostatų modulis"

#: kcm.cpp:38
#, kde-format
msgid "(C) 2015 Albert Vaca Cintora"
msgstr "(C) 2015 Albert Vaca Cintora"

#: kcm.cpp:42
#, kde-format
msgid "Albert Vaca Cintora"
msgstr "Albert Vaca Cintora"

#: kcm.cpp:232
#, kde-format
msgid "Key: %1"
msgstr "Raktas: %1"

#: kcm.cpp:257
#, kde-format
msgid "Available plugins"
msgstr "Prieinami papildiniai"

#: kcm.cpp:310
#, kde-format
msgid "Error trying to pair: %1"
msgstr "Klaida bandant suporuoti: %1"

#: kcm.cpp:331
#, kde-format
msgid "(paired)"
msgstr "(suporuotas)"

#: kcm.cpp:334
#, kde-format
msgid "(not paired)"
msgstr "(nesuporuotas)"

#: kcm.cpp:337
#, kde-format
msgid "(incoming pair request)"
msgstr "(gaunama suporavimo užklausa)"

#: kcm.cpp:340
#, kde-format
msgid "(pairing requested)"
msgstr "(užklausta suporavimo)"

#. i18n: ectx: property (text), widget (QLabel, rename_label)
#: kcm.ui:47
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#. i18n: ectx: property (text), widget (QToolButton, renameShow_button)
#: kcm.ui:70
#, kde-format
msgid "Edit"
msgstr "Taisyti"

#. i18n: ectx: property (text), widget (QToolButton, renameDone_button)
#: kcm.ui:92
#, kde-format
msgid "Save"
msgstr "Įrašyti"

#. i18n: ectx: property (text), widget (QPushButton, refresh_button)
#: kcm.ui:108
#, kde-format
msgid "Refresh"
msgstr "Įkelti iš naujo"

#. i18n: ectx: property (text), widget (QLabel, name_label)
#: kcm.ui:181
#, kde-format
msgid "Device"
msgstr "Įrenginys"

#. i18n: ectx: property (text), widget (QLabel, status_label)
#: kcm.ui:197
#, kde-format
msgid "(status)"
msgstr "(būsena)"

#. i18n: ectx: property (text), widget (QLabel, verificationKey)
#: kcm.ui:220
#, kde-format
msgid "🔑 abababab"
msgstr "🔑 abababab"

#. i18n: ectx: property (text), widget (QPushButton, accept_button)
#: kcm.ui:261
#, kde-format
msgid "Accept"
msgstr "Priimti"

#. i18n: ectx: property (text), widget (QPushButton, reject_button)
#: kcm.ui:268
#, kde-format
msgid "Reject"
msgstr "Atmesti"

#. i18n: ectx: property (text), widget (QPushButton, pair_button)
#: kcm.ui:281
#, kde-format
msgid "Request pair"
msgstr "Užklausti suporavimo"

#. i18n: ectx: property (text), widget (QPushButton, unpair_button)
#: kcm.ui:294
#, kde-format
msgid "Unpair"
msgstr "Išporuoti"

#. i18n: ectx: property (text), widget (QPushButton, ping_button)
#: kcm.ui:307
#, kde-format
msgid "Send ping"
msgstr "Siųsti ryšio patikrinimą"

#. i18n: ectx: property (text), widget (QLabel, noDeviceLinks)
#: kcm.ui:345
#, fuzzy, kde-format
#| msgid ""
#| "<html><head/><body><p>No device selected.<br><br>If you own an Android "
#| "device, make sure to install the <a href=\"https://play.google.com/store/"
#| "apps/details?id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: "
#| "underline; color:#4c6b8a;\">KDE Connect Android app</span></a> (also "
#| "available <a href=\"https://f-droid.org/repository/browse/?fdid=org.kde."
#| "kdeconnect_tp\"><span style=\" text-decoration: underline; color:#4c6b8a;"
#| "\">from F-Droid</span></a>) and it should appear in the list.<br><br>If "
#| "you are having problems, visit the <a href=\"https://community.kde.org/"
#| "KDEConnect\"><span style=\" text-decoration: underline; color:#4c6b8a;"
#| "\">KDE Connect Community wiki</span></a> for help.</p></body></html>"
msgid ""
"<html><head/><body><p>No device selected.<br><br>If you own an Android "
"device, make sure to install the <a href=\"https://play.google.com/store/"
"apps/details?id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: "
"underline; color:#4c6b8a;\">KDE Connect Android app</span></a> (also "
"available <a href=\"https://f-droid.org/repository/browse/?fdid=org.kde."
"kdeconnect_tp\"><span style=\" text-decoration: underline; color:#4c6b8a;"
"\">from F-Droid</span></a>) and it should appear in the list.<br><br>If you "
"are having problems, visit the <a href=\"https://userbase.kde.org/KDEConnect"
"\"><span style=\" text-decoration: underline; color:#4c6b8a;\">KDE Connect "
"Community wiki</span></a> for help.</p></body></html>"
msgstr ""
"<html><head/><body><p>Nepasirinktas joks įrenginys.<br><br>Jei turite "
"Android įrenginį, įsitikinkite, kad esate įdiegę <a href=\"https://play."
"google.com/store/apps/details?id=org.kde.kdeconnect_tp\"><span style=\" text-"
"decoration: underline; color:#4c6b8a;\">KDE Connect Android programėlę</"
"span></a> (taip pat prieinama <a href=\"https://f-droid.org/repository/"
"browse/?fdid=org.kde.kdeconnect_tp\"><span style=\" text-decoration: "
"underline; color:#4c6b8a;\">iš F-Droid</span></a>) ir jis turėtų atsirasti "
"sąraše.<br><br>Jei iškyla problemų, apsilankykite <a href=\"https://"
"community.kde.org/KDEConnect\"><span style=\" text-decoration: underline; "
"color:#4c6b8a;\">KDE Connect bendruomenės vikyje</span></a>.</p></body></"
"html>"

#~ msgid "(C) 2018 Nicolas Fella"
#~ msgstr "(C) 2018 Nicolas Fella"

#~ msgid "Plugins"
#~ msgstr "Įskiepiai"

#~ msgid "Browse"
#~ msgstr "Naršyti"
