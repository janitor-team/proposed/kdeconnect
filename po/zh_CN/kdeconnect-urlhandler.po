msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-10-12 00:16+0000\n"
"PO-Revision-Date: 2022-02-26 05:19\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-stable/messages/kdeconnect-kde/kdeconnect-urlhandler."
"pot\n"
"X-Crowdin-File-ID: 12124\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "KDE 中国"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-china@kde.org"

#: kdeconnect-handler.cpp:42
#, kde-format
msgid "KDE Connect URL handler"
msgstr "KDE Connect URL 处理程序"

#: kdeconnect-handler.cpp:48
#, kde-format
msgid "(C) 2017 Aleix Pol Gonzalez"
msgstr "(C) 2017 Aleix Pol Gonzalez"

#: kdeconnect-handler.cpp:64
#, kde-format
msgid "URL to share"
msgstr "要分享的 URL"

#: kdeconnect-handler.cpp:65
#, kde-format
msgid "Select a device"
msgstr "选择设备"

#: kdeconnect-handler.cpp:65
#, kde-format
msgid "id"
msgstr "ID"

#: kdeconnect-handler.cpp:101
#, kde-format
msgid "Enter URL here"
msgstr "在此处输入 URL"

#: kdeconnect-handler.cpp:109
#, kde-format
msgid "Enter file location here"
msgstr "在此处输入文件位置"

#: kdeconnect-handler.cpp:130
#, kde-format
msgid "Device to call %1 with:"
msgstr "用于呼叫 %1 的设备："

#: kdeconnect-handler.cpp:133
#, kde-format
msgid "Device to open %1 on:"
msgstr "用于打开 %1 的设备："

#: kdeconnect-handler.cpp:136
#, kde-format
msgid "Device to send a SMS with:"
msgstr "用于发送短信的设备："

#: kdeconnect-handler.cpp:139
#, kde-format
msgid "Device to send %1 to:"
msgstr "用于发送 %1 的设备："

#: kdeconnect-handler.cpp:168
#, kde-format
msgid "Couldn't share %1"
msgstr "无法分享 %1"
