# Translation of kdeconnect-indicator.po to Catalan (Valencian)
# Copyright (C) 2014-2021 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-10-20 00:17+0000\n"
"PO-Revision-Date: 2021-06-18 10:38+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 21.04.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Antoni Bella"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "antonibella5@yahoo.com"

#: deviceindicator.cpp:49
#, kde-format
msgid "Browse device"
msgstr "Navega per aquest dispositiu"

#: deviceindicator.cpp:58
#, kde-format
msgid "Ring device"
msgstr "Truca al dispositiu"

#: deviceindicator.cpp:67
#, kde-format
msgid "Get a photo"
msgstr "Aconsegueix una foto"

#: deviceindicator.cpp:78
#, kde-format
msgid "Send a file/URL"
msgstr "Envia un fitxer/URL"

#: deviceindicator.cpp:88
#, kde-format
msgid "SMS Messages..."
msgstr "Missatges SMS..."

#: deviceindicator.cpp:96
#, kde-format
msgid "Run command"
msgstr "Executa una ordre"

#: deviceindicator.cpp:98
#, kde-format
msgid "Add commands"
msgstr "Afig ordres"

#: indicatorhelper_mac.cpp:32
#, kde-format
msgid "Launching"
msgstr "S'està llançant"

#: indicatorhelper_mac.cpp:69
#, kde-format
msgid "Launching daemon"
msgstr "S'està llançant el dimoni"

#: indicatorhelper_mac.cpp:75 indicatorhelper_mac.cpp:103
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: indicatorhelper_mac.cpp:76
#, kde-format
msgid "Cannot find kdeconnectd"
msgstr "No s'ha pogut trobar el «kdeconnectd»"

#: indicatorhelper_mac.cpp:84
#, kde-format
msgid "Waiting D-Bus"
msgstr "S'està a l'espera del D-Bus"

#: indicatorhelper_mac.cpp:104
#, kde-format
msgid ""
"Cannot connect to DBus\n"
"KDE Connect will quit"
msgstr ""
"No s'ha pogut connectar amb el D-Bus\n"
"El KDE Connect eixirà"

#: indicatorhelper_mac.cpp:115
#, kde-format
msgid "Loading modules"
msgstr "S'estan carregant els mòduls"

#: main.cpp:40
#, kde-format
msgid "KDE Connect Indicator"
msgstr "Indicador del KDE Connect"

#: main.cpp:42
#, kde-format
msgid "KDE Connect Indicator tool"
msgstr "Eina Indicador del KDE Connect"

#: main.cpp:44
#, kde-format
msgid "(C) 2016 Aleix Pol Gonzalez"
msgstr "(C) 2016 Aleix Pol Gonzalez"

#: main.cpp:79
#, kde-format
msgid "Configure..."
msgstr "Configura..."

#: main.cpp:99
#, kde-format
msgid "Pairing requests"
msgstr "Sol·licituds d'aparellament"

#: main.cpp:104
#, kde-format
msgid "Pair"
msgstr "Parell"

#: main.cpp:105
#, kde-format
msgid "Reject"
msgstr "Rebutja"

#: main.cpp:111 main.cpp:125
#, kde-format
msgid "Quit"
msgstr "Ix"

#: main.cpp:142 main.cpp:164
#, kde-format
msgid "%1 device connected"
msgid_plural "%1 devices connected"
msgstr[0] "%1 dispositiu connectat"
msgstr[1] "%1 dispositius connectats"

#: systray_actions/battery_action.cpp:28
#, kde-format
msgid "No Battery"
msgstr "Sense bateria"

#: systray_actions/battery_action.cpp:30
#, kde-format
msgid "Battery: %1% (Charging)"
msgstr "Bateria: %1% (està carregant)"

#: systray_actions/battery_action.cpp:32
#, kde-format
msgid "Battery: %1%"
msgstr "Bateria: %1%"

#: systray_actions/connectivity_action.cpp:30
#, kde-format
msgctxt ""
"The fallback text to display in case the remote device does not have a "
"cellular connection"
msgid "No Cellular Connectivity"
msgstr "Sense connectivitat cel·lular"

#: systray_actions/connectivity_action.cpp:32
#, kde-format
msgctxt ""
"Display the cellular connection type and an approximate percentage of signal "
"strength"
msgid "%1  | ~%2%"
msgstr "%1  | ~%2%"
