# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# Volkan Gezer <volkangezer@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-09 00:16+0000\n"
"PO-Revision-Date: 2021-03-23 12:48+0100\n"
"Last-Translator: Volkan Gezer <volkangezer@gmail.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.08.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Volkan Gezer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "volkangezer@gmail.com"

#: main.cpp:27
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: main.cpp:27
#, kde-format
msgid "(c) 2015, Aleix Pol Gonzalez"
msgstr "(c) 2015, Aleix Pol Gonzalez"

#: main.cpp:28
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: main.cpp:28
#, kde-format
msgid "Maintainer"
msgstr "Bakımcı"

#: main.cpp:44
#, kde-format
msgid "URL to share"
msgstr ""

#: qml/DevicePage.qml:24
#, kde-format
msgid "Unpair"
msgstr "Eşleşmeyi kaldır"

#: qml/DevicePage.qml:29
#, kde-format
msgid "Send Ping"
msgstr "Ping Gönder"

#: qml/DevicePage.qml:37 qml/PluginSettings.qml:16
#, kde-format
msgid "Plugin Settings"
msgstr "Eklenti Ayarları"

#: qml/DevicePage.qml:63
#, kde-format
msgid "Multimedia control"
msgstr "Multimedya kontrolü"

#: qml/DevicePage.qml:70
#, kde-format
msgid "Remote input"
msgstr "Uzaktan giriş"

#: qml/DevicePage.qml:77 qml/presentationRemote.qml:15
#, kde-format
msgid "Presentation Remote"
msgstr "Sunum Uzaktan Kumandası"

#: qml/DevicePage.qml:86 qml/mousepad.qml:44
#, kde-format
msgid "Lock"
msgstr "Kilitle"

#: qml/DevicePage.qml:86
#, kde-format
msgid "Unlock"
msgstr "Kilidini aç"

#: qml/DevicePage.qml:93
#, kde-format
msgid "Find Device"
msgstr "Cihaz Bul"

#: qml/DevicePage.qml:98 qml/runcommand.qml:16
#, kde-format
msgid "Run command"
msgstr "Komut çalıştır"

#: qml/DevicePage.qml:106
#, kde-format
msgid "Share File"
msgstr "Dosya Paylaş"

#: qml/DevicePage.qml:111 qml/volume.qml:16
#, kde-format
msgid "Volume control"
msgstr "Ses kontrol"

#: qml/DevicePage.qml:120
#, kde-format
msgid "This device is not paired"
msgstr "Bu cihaz eşlenmemiş"

#: qml/DevicePage.qml:124 qml/FindDevicesPage.qml:23
#, kde-format
msgid "Pair"
msgstr "Eşleştir"

#: qml/DevicePage.qml:136
#, kde-format
msgid "Pair requested"
msgstr "Eşleşme isteği"

#: qml/DevicePage.qml:142
#, kde-format
msgid "Accept"
msgstr "Kabul et"

#: qml/DevicePage.qml:148
#, kde-format
msgid "Reject"
msgstr "Reddet"

#: qml/DevicePage.qml:157
#, kde-format
msgid "This device is not reachable"
msgstr "Bu cihaz erişilebilir değil"

#: qml/DevicePage.qml:165
#, kde-format
msgid "Please choose a file"
msgstr "Lütfen bir dosya seçin"

#: qml/FindDevicesPage.qml:38
#, kde-format
msgid "No devices found"
msgstr "Hiçbir cihaz bulunamadı"

#: qml/FindDevicesPage.qml:51
#, kde-format
msgid "Remembered"
msgstr "Hatırlanıyor"

#: qml/FindDevicesPage.qml:53
#, kde-format
msgid "Available"
msgstr "Mevcut"

#: qml/FindDevicesPage.qml:55
#, kde-format
msgid "Connected"
msgstr "Bağlı"

#: qml/main.qml:28
#, kde-format
msgid "Find devices..."
msgstr "Cihazları bul..."

#: qml/mousepad.qml:16
#, kde-format
msgid "Remote Control"
msgstr "Uzaktan Kumanda"

#: qml/mousepad.qml:59
#, kde-format
msgid "Press the left and right mouse buttons at the same time to unlock"
msgstr ""

#: qml/mpris.qml:19
#, kde-format
msgid "Multimedia Controls"
msgstr "Multimedya Kontrolleri"

#: qml/mpris.qml:61
#, kde-format
msgid "No players available"
msgstr "Oynatıcı bulunamadı"

#: qml/mpris.qml:109
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: qml/presentationRemote.qml:20
#, kde-format
msgid "Enable Full-Screen"
msgstr "Tam Ekranı Etkinleştir"

#: qml/runcommand.qml:21
#, kde-format
msgid "Edit commands"
msgstr "Komutları düzenle"

#: qml/runcommand.qml:24
#, kde-format
msgid "You can edit commands on the connected device"
msgstr "Bağlı cihazdaki komutları düzenleyebilirsiniz"

#: qml/runcommand.qml:43
#, kde-format
msgid "No commands defined"
msgstr "Tanımlanmış komut yok"
